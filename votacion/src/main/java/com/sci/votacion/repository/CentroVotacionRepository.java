package com.sci.votacion.repository;


import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import com.sci.votacion.model.CentroVotacion;

@Repository
@Transactional
public interface CentroVotacionRepository extends JpaRepository<CentroVotacion, Integer>, JpaSpecificationExecutor<CentroVotacion> {

	
}