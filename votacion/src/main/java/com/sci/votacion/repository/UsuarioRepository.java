package com.sci.votacion.repository;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import com.sci.votacion.model.Usuario;

@Repository
@Transactional
public interface UsuarioRepository extends JpaRepository<Usuario, Integer>, JpaSpecificationExecutor<Usuario> {

	//Page<Post> findAllByProfile(@Param("profile") Profile profile, Pageable pageable);

	//List<Post> findByProfile(Profile profile);

	//Page<Post> findAll(Pageable pageable);
	
	//List<Usuario> findByPersonaEmailBypassword();
	
	 Optional<Usuario> findUsuarioByUsername(String username);
	
}
