package com.sci.votacion.repository;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;

import com.sci.votacion.model.Persona;
import com.sci.votacion.model.Votante;

@Repository
public interface VotanteRepository extends JpaRepository<Votante, Integer>, JpaSpecificationExecutor<Votante> {
	
	Page<Votante> findAllByJefe(@Param("jefe") Persona jefe, Pageable pageable);
	
	List<Votante> findByJefe(Persona jefe);
	
	
	Page<Votante> findAll(Pageable pageable);
	
	
}

