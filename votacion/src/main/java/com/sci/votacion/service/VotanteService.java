package com.sci.votacion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sci.votacion.dto.VotanteDTO;
import com.sci.votacion.model.CentroVotacion;
import com.sci.votacion.model.Persona;
import com.sci.votacion.model.Usuario;
import com.sci.votacion.model.Votante;
import com.sci.votacion.repository.CentroVotacionRepository;
import com.sci.votacion.repository.PersonaRepository;
import com.sci.votacion.repository.VotanteRepository;

@Service("VotanteService")
public class VotanteService implements IVotanteService {

	private static final String CREATED = "created";

	@Autowired
	IUsuarioService iUsuario;

	@Autowired
	CentroVotacionRepository centro;
	
	@Autowired
	PersonaRepository perRepo;
	
	@Autowired
	VotanteRepository votanteRepo;
	
	
	@Override
	@Transactional
	public String agregaVotante(VotanteDTO votanteDTO, String username) {
		
		Usuario user = iUsuario.findUsuarioByUsername(username);
		Persona persona=perRepo.findByCedula(votanteDTO.getCedula());
		if (persona!=null)
			return("El votante: "+votanteDTO.getCedula()+" ya está registrado");
		persona=new Persona();
		persona.setApellidos(votanteDTO.getApellidos());
		persona.setNombres(votanteDTO.getNombres());
		persona.setCedula(votanteDTO.getCedula());
		persona.setStatus(1);
		persona.setTelefono(votanteDTO.getTelefonos());
		perRepo.save(persona);
		Votante votante=new Votante();
		CentroVotacion cv=centro.getOne(Integer.parseInt(votanteDTO.getCentros()));
		votante.setCentroVotacion(cv);
		votante.setPersona(persona);
		votante.setCambio(votanteDTO.getCambio());
		votante.setJefe(user.getPersona());
		votante.setStatus(1);
		votanteRepo.save(votante);
		return null;
	}

	@Override
	public String modificaVotante(VotanteDTO votanteDTO, String user) {
		// TODO Auto-generated method stub
		System.out.println("llegueamofdididiidjhdgbdg");
		return null;
	}

	@Override
	public String eliminaVotante(VotanteDTO votanteDTO, String user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Votante> findAll(String username, int pageNumber, int pageSize) {
		
		Usuario user = iUsuario.findUsuarioByUsername(username);
		Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(Order.desc(CREATED)));
		Page<Votante> listado=null;
		List<Votante> listado2 = null;
		if (user.getRole()==0) {
			//listado = votanteRepo.findAllByJefe( user.getPersona(), pageable);
			listado2 = votanteRepo.findByJefe(user.getPersona());
		}
		else
			listado = votanteRepo.findAll(pageable);
		return listado2;
	}

}
