package com.sci.votacion.service;

import java.util.List;

import com.sci.votacion.model.CentroVotacion;

public interface ICentroVotacionService {
	public List<CentroVotacion> findAll();
}
