package com.sci.votacion.service;

import java.util.List;

import com.sci.votacion.dto.VotanteDTO;
import com.sci.votacion.model.Votante;

public interface IVotanteService {
	
	String agregaVotante(VotanteDTO votanteDTO, String user);

	String modificaVotante(VotanteDTO votanteDTO, String user);

	String eliminaVotante(VotanteDTO votanteDTO, String user);
	
	List<Votante> findAll(String user, int pageNumber, int pageSize);
}
