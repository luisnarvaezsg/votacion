package com.sci.votacion.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sci.votacion.model.Usuario;
import com.sci.votacion.repository.UsuarioRepository;

@Service(value = "UsuarioService")
public class UsuarioService implements IUsuarioService, UserDetailsService {

	@Autowired
	UsuarioRepository usuarioRepository;

	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Usuario user = findUsuarioByUsername(username);
		System.out.println("es elseeeeeeee: " + user.getUsername() + " passwd: " + user.getPassword());
		String password = "{noop}" + user.getPassword();
		return new org.springframework.security.core.userdetails.User(user.getUsername(), password, getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("USER"));
	}

	@Override
	public Usuario findUsuarioByUsername(String username) throws UsernameNotFoundException {
		Usuario user = usuarioRepository.findUsuarioByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User not present"));
		return user;
	}

}
