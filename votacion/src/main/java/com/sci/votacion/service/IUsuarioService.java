package com.sci.votacion.service;


import com.sci.votacion.model.Usuario;

public interface IUsuarioService {
	
	
	Usuario findUsuarioByUsername(String username);
}
