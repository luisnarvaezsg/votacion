package com.sci.votacion.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sci.votacion.model.CentroVotacion;
import com.sci.votacion.repository.CentroVotacionRepository;

@Service("CentroVotacionService")
public class CentroVotacionService implements ICentroVotacionService {

	@Autowired
	CentroVotacionRepository centroVotacionRepository;
	
	@Override
	public List<CentroVotacion> findAll() {
		// TODO Auto-generated method stub
		return centroVotacionRepository.findAll();
	}

}
