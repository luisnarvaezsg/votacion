package com.sci.votacion.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
public class VotanteDTO {
	
	private String cedula;
	
	private String nombres;
	
	private String apellidos;
	
	private String telefonos;
	
	private String cambio;
	
	private String centros;
	
}
