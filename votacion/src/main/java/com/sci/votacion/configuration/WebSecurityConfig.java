package com.sci.votacion.configuration;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/resources/**").permitAll().antMatchers("/", "/home").permitAll()
				.anyRequest().authenticated().and()
				.formLogin().loginPage("/login").defaultSuccessUrl("/registro/agregar",true)
				.permitAll().and()
				.logout().invalidateHttpSession(true).clearAuthentication(true)
				.permitAll();
	}
//  A data breach on a site or app exposed yout password
	@Override
	public void configure(final WebSecurity web) throws Exception {

		web.ignoring().antMatchers("/favicon.ico", "/img/**", "/fonts/**", "/css/**", "/js/**", "/webjars/**",
				"/images/**");
	}

	/**
	 * @Bean
	 * @Override public UserDetailsService userDetailsService() { UserDetails user =
	 *           User.withDefaultPasswordEncoder() .username("user")
	 *           .password("password") .roles("USER") .build();
	 * 
	 *           return new InMemoryUserDetailsManager(user); }
	 **/

	@Resource(name = "UsuarioService")
	private UserDetailsService userDetailsService;

	@Autowired
	public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}
}