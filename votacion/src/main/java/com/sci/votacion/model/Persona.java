package com.sci.votacion.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * The persistent class for the persona database table.
 * 
 */
@Setter
@Getter
@Accessors(chain = true)
@Entity
@Table(name = "persona")
@SQLDelete(sql = "UPDATE votacion.persona SET status = 0 WHERE persona_id = ?")
@Where(clause = "status <> 0")
@NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p")
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "persona_id_generator")
	@SequenceGenerator(name = "persona_id_generator", sequenceName = "persona_persona_id_seq", allocationSize = 1)
	@Column(name = "persona_id")
	private Integer personaId;

	private String apellidos;

	private String cedula;

	private String nombres;

	@Column(name = "persona_email")
	private String personaEmail;

	private Integer status;

	private String telefono;


	public Persona() {
	}

	//bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy="persona")
	private List<Usuario> usuarios;

	// bi-directional many-to-one association to Votante
	@OneToMany(mappedBy = "jefe")
	private List<Votante> votantes1;

	// bi-directional many-to-one association to Votante
	@OneToMany(mappedBy = "persona")
	private List<Votante> votantes2;

	// bi-directional many-to-one association to Votante
	@OneToMany(mappedBy = "personaDelete")
	private List<Votante> votantes3;

	public Usuario addUsuario(Usuario usuario) {
		getUsuarios().add(usuario);
		usuario.setPersona(this);

		return usuario;
	}

	public Usuario removeUsuario(Usuario usuario) {
		getUsuarios().remove(usuario);
		usuario.setUsername(null);

		return usuario;
	}
	
	public Votante addVotantes1(Votante votantes1) {
		getVotantes1().add(votantes1);
		votantes1.setPersona(this);

		return votantes1;
	}

	public Votante removeVotantes1(Votante votantes1) {
		getVotantes1().remove(votantes1);
		votantes1.setPersona(null);

		return votantes1;
	}
	
	public Votante addVotantes2(Votante votantes2) {
		getVotantes2().add(votantes2);
		votantes2.setPersona(this);

		return votantes2;
	}

	public Votante removeVotantes2(Votante votantes2) {
		getVotantes2().remove(votantes2);
		votantes2.setPersona(null);

		return votantes2;
	}
	
	public Votante addVotantes3(Votante votantes3) {
		getVotantes3().add(votantes3);
		votantes3.setPersona(this);

		return votantes3;
	}

	public Votante removeVotantes3(Votante votantes3) {
		getVotantes3().remove(votantes3);
		votantes3.setPersona(null);

		return votantes3;
	}

	@Transient
	public String getNombreCompleto() {
		return getNombres()+" "+ getApellidos();
	}
	
}