package com.sci.votacion.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * The persistent class for the centro_votacion database table.
 * 
 */
@Setter
@Getter
@Accessors(chain = true)
@SQLDelete(sql = "UPDATE votacion.centro_votacion SET status = 0 WHERE persona_id = ?")
@Where(clause = "status <> 0")
@Entity
@Table(name="centro_votacion")
@NamedQuery(name="CentroVotacion.findAll", query="SELECT c FROM CentroVotacion c")
public class CentroVotacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="centro_votacion_id")
	private Integer centroVotacionId;
	private String codigo;

	private String direccion;

	private Integer electores;

	private Integer mesas;

	private String nombre;

	private Integer status;

	//bi-directional many-to-one association to Votante
	@OneToMany(mappedBy="centroVotacion")
	private List<Votante> votantes;

	public CentroVotacion() {
	}

	public Votante addVotante(Votante votante) {
		getVotantes().add(votante);
		votante.setCentroVotacion(this);

		return votante;
	}

	public Votante removeVotante(Votante votante) {
		getVotantes().remove(votante);
		votante.setCentroVotacion(null);

		return votante;
	}

}