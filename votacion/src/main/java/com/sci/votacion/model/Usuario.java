package com.sci.votacion.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

/**
 * The persistent class for the usuario database table.
 * 
 */
@Setter
@Getter
@Accessors(chain = true)
@Table(name = "usuario")
@SQLDelete(sql = "UPDATE votacion.usuario SET status = 0 WHERE usuario_id = ?")
@Where(clause = "status <> 0")
@Entity
@NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "usuario_id")
	private Integer usuarioId;

	private Timestamp created;

	@Column(name = "last_login")
	private Timestamp lastLogin;

	private String username;
	
	private String password;

	
	//bi-directional many-to-one association t
	@ManyToOne
	@JoinColumn(name="persona_id")
	private Persona persona;

	private Integer role;
		
	private Integer status;

	public Usuario() {
	}

}