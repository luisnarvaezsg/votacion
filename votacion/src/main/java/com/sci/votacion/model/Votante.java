package com.sci.votacion.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * The persistent class for the votante database table.
 * 
 */
@Setter
@Getter
@Accessors(chain = true)
@Table(name = "votante")
@SQLDelete(sql = "UPDATE votacion.votante SET status = 0 WHERE votante_id = ?")
@Where(clause = "status <> 0")
@Entity
@NamedQuery(name = "Votante.findAll", query = "SELECT v FROM Votante v")
public class Votante implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "votante_id_generator")
	@SequenceGenerator(name = "votante_id_generator", sequenceName = "votante_votante_id_seq", allocationSize = 1)
	@Column(name = "votante_id")
	private Integer votanteId;

	@Temporal(TemporalType.DATE)
	@Column(name="deleted_at")
	private Date deletedAt;
	
	private String cambio;
	
	private Integer status;

	// bi-directional many-to-one association to CentroVotacion
	@ManyToOne
	@JoinColumn(name = "centro_votacion_id")
	private CentroVotacion centroVotacion;

	// bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name = "jefe_id")
	private Persona jefe;

	// bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name = "persona_id")
	private Persona persona;

	// bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name = "persona_delete")
	private Persona personaDelete;

	public Votante() {
	}

	
	
	
}