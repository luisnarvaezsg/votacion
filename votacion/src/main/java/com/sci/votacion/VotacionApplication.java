package com.sci.votacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EnableJpaRepositories("com.sci.votacion.repository") 
@EntityScan("com.sci.votacion.model")
@SpringBootApplication
public class VotacionApplication {

	public static void main(String[] args) {
		SpringApplication.run(VotacionApplication.class, args);
	}

}
