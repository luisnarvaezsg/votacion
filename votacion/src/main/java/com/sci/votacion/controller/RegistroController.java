package com.sci.votacion.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sci.votacion.dto.VotanteDTO;
import com.sci.votacion.model.CentroVotacion;
import com.sci.votacion.model.Votante;
import com.sci.votacion.service.ICentroVotacionService;
import com.sci.votacion.service.IVotanteService;
@RequestMapping("/registro")
@Controller
public class RegistroController {

	@Autowired
	ICentroVotacionService ics;

	@Autowired
	IVotanteService iVotanteService;

	/**@RequestMapping(value = "/listado", method = RequestMethod.GET)
	public String votantePage(HttpServletRequest request, Model model,
			@RequestParam(value = "error", required = false) String error) {
		List<CentroVotacion> centros = ics.findAll();
		model.addAttribute("centros", centros);
		return "listado";
	}**/

	@RequestMapping(value = "/agregar", method = RequestMethod.GET)
	public String registro(Model model, @RequestParam(value = "mensaje", required = false) String mensaje) {
		List<CentroVotacion> centros = ics.findAll();
		VotanteDTO votante = new VotanteDTO();
		model.addAttribute("centros", centros);
		model.addAttribute("votanteDTO", votante);
		model.addAttribute("mensaje", mensaje);
		return "registro";
	}

	@RequestMapping(value = "/agregar", method = RequestMethod.POST)
	public String registroAgrega(VotanteDTO votanteDTO, HttpServletRequest request, Model model,
			@RequestParam(value = "error", required = false) String error) {
		String user = request.getRemoteUser();
		String mensaje = iVotanteService.agregaVotante(votanteDTO, user);
		// Manejo de errores
		if (mensaje == null)
			return "registro";
		else {
			if (mensaje.equals("No value present"))
				model.addAttribute("mensaje", "El usuario no existe");
			else {
				model.addAttribute("mensaje", mensaje);
			}
			return "registro";
		}
	}
	

	@RequestMapping(value = "/modifica", method = RequestMethod.GET)
	public String registroModifica(@RequestParam(name="id") String id, Model model) {
		System.out.println("Estoy enactualizarrerererrerer"+id);
		model.addAttribute("mensaje", "El usumodifica no existe");
			return "registro";
	}
	
	@RequestMapping(value = "/registro/id", method = RequestMethod.DELETE)
	public String registroBorrar(VotanteDTO votanteDTO, HttpServletRequest request, Model model,
			@RequestParam(value = "error", required = false) String error) {
			return "registro";
	}
	

	@RequestMapping(value = "/informacion", method = RequestMethod.GET)
	public String informacion(HttpServletRequest request, Model model,
			@RequestParam(value = "error", required = false) String error) {
		return "informacion";
	}
/**
 * @RequestParam(value = "pageNumber", required = true) Integer pageNumber,
					      @RequestParam(value = "pageSize", required = true) Integer pageSize,
 * @param request
 * @param model
 * @return
 */
	@RequestMapping(value = "/listado", method = RequestMethod.GET)
	public String listado( 
					      HttpServletRequest request, Model model) {
		String user = request.getRemoteUser();
		List<Votante> votantes = iVotanteService.findAll(user, 1, 1);
		model.addAttribute("votantes", votantes);
		return "listado";
	}
}
