package com.sci.votacion.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

	@GetMapping(value = "/login")
	public String login(HttpServletRequest request) {
		String user = request.getRemoteUser();
		if (user==null) {
			return "login";
		} else
			return "redirect:/logout";
	}
	
	@RequestMapping(value = "/hello", method = RequestMethod.POST)
    public String loginPage(@RequestParam(value = "username", required = true) String username,
    						@RequestParam(value = "password", required = true) String password,
    						@RequestParam(value = "error", required = false) String error,
                            @RequestParam(value = "logout", required = false) String logout,
                            Model model) {
		System.out.println("Estoy en LoginCIiiontrollwr"+username+" pass: "+password);
        String errorMessage = null;
        
        if(error != null) {
            errorMessage = "Username or Password is incorrect !!";
        }
        if(logout != null) {
            errorMessage = "You have been successfully logged out !!";
        }
      /**  if (!(new UsuarioService().validaUsuario(username, password))) {
        	model.addAttribute("error", "error");
        	System.out.println("Estoryenerrore");
        	//return "redirect:/login?error="+"Usuriaididaaaaaid";
        	return "login";
        }**/
    	System.out.println("vOY A VOTANTETETETE");
    	return "redirect:/votante";	
        
    }
	
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){   
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout=true";
    }
	



}
