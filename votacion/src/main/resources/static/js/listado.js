$(document).ready(function() {
	
    var oTable = $("#listadoVotantes").dataTable( {
    	"sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "sSwfPath": "./js/lib/copy_csv_xls_pdf.swf",
            "aButtons": [
                {
                    "sExtends": "xls",
                    "sTitle": "Reporte Flota",
                    "mColumns": [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                },
                {
                    "sExtends": "pdf",
                    "sTitle": "Reporte Flota",
                    "mColumns": [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
                }
            ]
        },
        "aaSorting": [],
        "bProcessing": true,
        "bPaginate": true,
        "sPaginationType": "full_numbers",
        "bFilter": true,
        "iDisplayLength": 12,
        "bSort": true,
        "oLanguage": {"sProcessing":     "Procesando...",
     		"sLengthMenu":     "Mostrar _MENU_ registros",
     		"sZeroRecords":    "No se encontraron resultados", 
    		 "sEmptyTable":     "Ning\u00FAn dato disponible en esta tabla", 
     		"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", 
     		"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros", 
    		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)", 
    		"sInfoPostFix":    "", 
     		"sSearch":         "Buscar:", 
    		"sUrl":            "", 
    		"sInfoThousands":  ",", 
     		"sLoadingRecords": "Cargando...", 
    		 	"oPaginate": { 
         		"sFirst":    "Primero", 
        		"sLast":     "Último", 
         		"sNext":     "Siguiente", 
         		"sPrevious": "Anterior" 
     			}, 
     		"fnInfoCallback": null, 
    		 "oAria": { 
         	 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente", 
        	 "sSortDescending": ": Activar para ordenar la columna de manera descendente" 
     		}
         }
    });
     
    $("tfoot input").keyup( function () {
        /* Filter on the column (the index) of this element */
        oTable.fnFilter( this.value, $("tfoot input").index(this) );
   	});



    /*
     * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
     * the footer
     */
    $("tfoot input").each( function (i) {
        asInitVals[i] = this.value;
    });
     
    $("tfoot input").focus( function () {
        if ( this.className == "search_init" )
        {
            this.className = "";
            this.value = "";
        }
    });
     
    $("tfoot input").blur( function (i) {
        if ( this.value == "" )
        {
            this.className = "search_init";
            this.value = asInitVals[$("tfoot input").index(this)];
        }
    });
   
    var table = $('#listadoPosts').DataTable();

	$("#modificar").click(function() {
	  	alert( "Handler for sraeraew.click() called." );
	  	$.ajax({
		    url : 'http://voicebunny.comeze.com/index.php',
		    type : 'GET',
		    data : {
		        'numberOfWords' : 10
		    },
		    dataType:'json',
		    success : function(data) {              
		        alert('Data: '+data);
		    },
		    error : function(request,error)
		    {
		        alert("Request: "+JSON.stringify(request));
		    }
		});
	});
	
	

});

function modificar2(cedula){
	alert("La cedulae s: "+cedula);
 	$.ajax({
	    url : 'modifica',
	    type : 'GET',
	    data : {
	        id: cedula
	    },
	    dataType:'json',
	    success : function(data) {              
	        alert('Data: '+data);
	    },
	    error : function(request,error)
	    {
	        alert("Request: "+JSON.stringify(request));
	    }
	});
}
